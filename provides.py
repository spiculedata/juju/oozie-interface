from charmhelpers.core import hookenv
from charms.reactive import hook
from charms.reactive import RelationBase
from charms.reactive import scopes
from charms.reactive import not_unless
from charmhelpers.core.hookenv import log


class OozieProvides(RelationBase):
    # Every unit connecting will get the same information
    scope = scopes.GLOBAL

    # Use some template magic to declare our relation(s)
    @hook('{provides:oozie}-relation-{joined}')
    def changed(self):
        # Signify that the relationship is now available to our principal layer(s)
        conversation = self.conversation()
        conversation.set_state('{relation_name}.config.requested')

    @hook('{provides:oozie}-relation-{broken,departed}')
    def broken(self):
        # Remove the state that our relationship is now available to our principal layer(s)
        conversation = self.conversation()
        conversation.remove_state('{relation_name}.config.requested')

    @not_unless('{provides:oozie}.config.requested')
    def provide_config(self, ip, port):
        """
        Provide a database to a requesting service.
        :param str service: The service which requested the database, as
            returned by :meth:`~provides.MySQL.requested_databases`.
        :param str host: The host where the database can be reached (e.g.,
            the charm's private or public-address).
        :param int port: The port where the database can be reached.
        :param str database: The name of the database being provided.
        :param str user: The username to be used to access the database.
        :param str password: The password to be used to access the database.
        """
        log("providing connection "+ip)
        conversation = self.conversation()
        conversation.set_remote(
            ip=ip,
            port=port
        )
        conversation.remove_state('{relation_name}.config.requested')
