from charms.reactive import hook
from charms.reactive import RelationBase
from charms.reactive import scopes
from charmhelpers.core.hookenv import log

class OozieRequires(RelationBase):
    scope = scopes.GLOBAL

    auto_accessors = ['ip','port']


    @hook('{requires:oozie}-relation-{joined,changed}')
    def changed(self):
        self.set_state('{relation_name}.connected')
        self.set_state('{relation_name}.available')

    @hook('{requires:oozie}-relation-{departed,broken}')
    def broken(self):
        self.remove_state('{relation_name}.connected')
        self.remove_state('{relation_name}.available')

    def connection_string(self):
        """
        Get the connection string, if available, or None.
        """
        return self.ip()
